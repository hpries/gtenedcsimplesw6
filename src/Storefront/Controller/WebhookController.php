<?php
namespace GTen\EDCSimple\Storefront\Controller;

use GTen\EDCSimple\Services\TrackingNumberService;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WebhookController extends StorefrontController {
    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    /** @var TrackingNumberService */
    private $service;

    public function __construct(TrackingNumberService $service)
    {
        $this->service = $service;
    }

    public function injectConfig(SystemConfigService $systemConfigService) {
        $this->systemConfigService = $systemConfigService;
    }

    private function getData(): string
    {
        $raw = urldecode(file_get_contents('php://input'));
        $raw = preg_replace("/^[^<]+(<.+)$/Uis", '$1', $raw);
        $raw = preg_replace("/^(.+>)[^>]+$/Uis", '$1', $raw);
        return trim($raw);
    }

    /**
     * @Route("/edc/webhook/validate", name="frontend.edc.webhook.valid", methods={"GET"}, defaults={"csrf_protected"=false, "_routeScope"={"storefront"}})
     */
    public function info(Request $request): Response {
        $token = $this->systemConfigService->get('GTenEDCSimple.config.edcsecuritytoken');
        $currentToken = $request->get('token');

        $result = '<result>' . ( $token == $currentToken ? 'valid' : 'invalid') . '</result>';

        return new Response($result, 200, ['Content-Type' => 'text/xml']);
    }

    /**
     * @Route("/edc/webhook/push", name="frontend.edc.webhook.push", methods={"POST"}, defaults={"csrf_protected"=false, "_routeScope"={"storefront"}})
     */
    public function push(Request $request): Response {
        $token = $this->systemConfigService->get('GTenEDCSimple.config.edcsecuritytoken');
        $code = 401;
        $result = '<result><success>failed</success><error>wrong token</error></result>';
        if ($request->get('data') && $request->get('token') && $request->get('token') == $token) {
            $simpleXML = simplexml_load_string($this->getData());
            $number = $simpleXML->own_ordernumber;
            $trackingCode = $simpleXML->tracktrace;

            if(strlen($number) > 0){
                $this->service->updateOrder($number, $trackingCode);
                $result = '<result><success>ok</success><error></error></result>';
                $code = 200;
            }
            else {
                $result = '<result><success>failed</success><error>no number</error></result>';
                $code = 400;
            }
        }
        return new Response($result, $code, ['Content-Type' => 'text/xml']);
    }
}