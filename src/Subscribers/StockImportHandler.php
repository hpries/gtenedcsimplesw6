<?php
namespace GTen\EDCSimple\Subscribers;

use Exception;
use GTen\EDCSimple\Services\InStockService;
use GTen\EDCSimple\Tasks\StockImportTask;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;

class StockImportHandler extends ScheduledTaskHandler {
    /**
     * @var InStockService
     */
    private $service;

    public function __construct(EntityRepositoryInterface $scheduledTaskRepository, InStockService $service)
    {
        $this->service = $service;
        parent::__construct($scheduledTaskRepository);
    }

    public static function getHandledMessages(): iterable
    {
        return [ StockImportTask::class];
    }

    public function run(): void
    {
        try {
            $this->service->run();
        }
        catch (Exception $e) {
            $this->service->logError($e);
        }
    }
}