<?php
namespace GTen\EDCSimple\Subscribers;

use Exception;
use GTen\EDCSimple\Services\OrderExportService;
use GTen\EDCSimple\Tasks\OrderExportTask;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;

class OrderExportHandler extends ScheduledTaskHandler {
    /**
     * @var OrderExportService
     */
    private $service;

    public function __construct(EntityRepositoryInterface $scheduledTaskRepository, OrderExportService $service)
    {
        $this->service = $service;
        parent::__construct($scheduledTaskRepository);
    }

    public static function getHandledMessages(): iterable
    {
        return [ OrderExportTask::class];
    }

    public function run(): void
    {
        try {
            $this->service->run();
        }
        catch (Exception $e) {
            $this->service->logError($e);
        }
    }
}