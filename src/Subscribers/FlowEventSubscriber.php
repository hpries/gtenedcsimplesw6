<?php
namespace GTen\EDCSimple\Subscribers;

use GTen\EDCSimple\Subscribers\Events\ErrorEDCOrder;
use GTen\EDCSimple\Subscribers\Events\NoneEDCOrder;
use GTen\EDCSimple\Subscribers\Events\SuccessEDCOrder;
use Shopware\Core\Framework\Event\BusinessEventCollector;
use Shopware\Core\Framework\Event\BusinessEventCollectorEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FlowEventSubscriber implements  EventSubscriberInterface {
    private BusinessEventCollector $businessEventCollector;

    public function __construct(BusinessEventCollector $businessEventCollector) {
        $this->businessEventCollector = $businessEventCollector;
    }

    public static function getSubscribedEvents()
    {
        return [
            BusinessEventCollectorEvent::NAME => ['onEvent', 1000],
        ];
    }

    public function onEvent(BusinessEventCollectorEvent $event): void
    {
        $definitions = [];
        $definitions[] = $this->businessEventCollector->define(ErrorEDCOrder::class);
        $definitions[] = $this->businessEventCollector->define(NoneEDCOrder::class);
        $definitions[] = $this->businessEventCollector->define(SuccessEDCOrder::class);

        $collection = $event->getCollection();
        foreach ($definitions as $definition) {
            if($definition) {
                $collection->set($definition->getName(), $definition);
            }
        }
    }
}