<?php
namespace GTen\EDCSimple\Subscribers;

use Exception;
use GTen\EDCSimple\Services\ProductImportService;
use GTen\EDCSimple\Tasks\DailyImportTask;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class DailyImportHandler extends ScheduledTaskHandler {
    /**
     * @var ProductImportService
     */
    private $service;

    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    public function __construct(EntityRepositoryInterface $scheduledTaskRepository, ProductImportService $service, SystemConfigService $systemConfigService)
    {
        $this->service = $service;
        $this->systemConfigService = $systemConfigService;
        parent::__construct($scheduledTaskRepository);
    }

    public static function getHandledMessages(): iterable
    {
        return [ DailyImportTask::class];
    }

    public function run(): void
    {
        try {
            $onlyAllowedInGerman = $this->systemConfigService->get('GTenEDCSimple.config.edcImportRestrictions');

            $url = trim($this->systemConfigService->get('GTenEDCSimple.config.edcDailyImportUrl'));
            $xml = simplexml_load_string(file_get_contents($url));
            $products = [];
            if($xml->product->count() == 1) {
                $products = [$xml->product];
            }
            else {
                $products = $xml->product;
            }

            foreach ($products as $productXML) {
                $this->service->importByXML($productXML, $onlyAllowedInGerman);
            }
        } catch (Exception $e) {
            $this->service->logError($e);
        }
    }
}