<?php
namespace GTen\EDCSimple\Subscribers\Events;

class ErrorEDCOrder extends EdcOrderGeneral {

    public function getName(): string
    {
        return 'edc.order.export.error';
    }
}