<?php
namespace GTen\EDCSimple\Subscribers\Events;

use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Event\CustomerAware;
use Shopware\Core\Framework\Event\EventData\ArrayType;
use Shopware\Core\Framework\Event\EventData\EntityType;
use Shopware\Core\Framework\Event\EventData\EventDataCollection;
use Shopware\Core\Framework\Event\EventData\MailRecipientStruct;
use Shopware\Core\Framework\Event\EventData\ScalarValueType;
use Shopware\Core\Framework\Event\MailAware;
use Shopware\Core\Framework\Event\OrderAware;
use Shopware\Core\Framework\Event\SalesChannelAware;

abstract class EdcOrderGeneral implements MailAware, SalesChannelAware, CustomerAware, OrderAware {

    private OrderEntity $order;
    private Context $context;
    private array $options;

    public function __construct(OrderEntity $order, Context $context, array $options = [])
    {
        $this->order = $order;
        $this->context = $context;
        $this->options = $options;
    }

    public function getCustomerId(): string
    {
        return $this->order->getOrderCustomer()->getCustomerId();
    }

    public static function getAvailableData(): EventDataCollection
    {
        return ((new EventDataCollection())
            ->add('order', new EntityType(OrderEntity::class))
            ->add('options', new ArrayType(new ScalarValueType(ScalarValueType::TYPE_STRING)))
        );
    }

    abstract public function getName(): string;

    public function getMailStruct(): MailRecipientStruct
    {
        return new MailRecipientStruct(
            [$this->order->getOrderCustomer()->getEmail()]
        );
    }

    public function getSalesChannelId(): string
    {
        return $this->order->getSalesChannelId();
    }

    public function getOrderId(): string
    {
        return $this->order->getId();
    }

    public function getContext(): Context
    {
        return $this->context;
    }

    /**
     * @return OrderEntity
     */
    public function getOrder(): OrderEntity
    {
        return $this->order;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}