<?php
namespace GTen\EDCSimple\Subscribers\Events;

class NoneEDCOrder extends EdcOrderGeneral {

    public function getName(): string
    {
        return 'edc.order.export.none';
    }
}