<?php
namespace GTen\EDCSimple\Subscribers\Events;

class SuccessEDCOrder extends EdcOrderGeneral {

    public function getName(): string
    {
        return 'edc.order.export.success';
    }
}