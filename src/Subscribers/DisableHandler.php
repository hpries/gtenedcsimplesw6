<?php
namespace GTen\EDCSimple\Subscribers;

use Exception;
use GTen\EDCSimple\Services\InStockService;
use GTen\EDCSimple\Services\ProductDisableService;
use GTen\EDCSimple\Tasks\DisableTask;
use GTen\EDCSimple\Tasks\StockImportTask;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;

class DisableHandler extends ScheduledTaskHandler {
    /**
     * @var ProductDisableService
     */
    private $service;

    public function __construct(EntityRepositoryInterface $scheduledTaskRepository, ProductDisableService $service)
    {
        $this->service = $service;
        parent::__construct($scheduledTaskRepository);
    }

    public static function getHandledMessages(): iterable
    {
        return [ DisableTask::class];
    }

    public function run(): void
    {
        try {
            $this->service->disableOld();
        }
        catch (Exception $e) {
            $this->service->logError($e);
        }
    }
}