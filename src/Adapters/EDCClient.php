<?php

namespace GTen\EDCSimple\Adapters;

use Exception;

class EDCClient
{
    const METHOD_GET = 'GET';
    const METHOD_PUT = 'PUT';
    const METHOD_POST = 'POST';
    const METHOD_DELETE = 'DELETE';
    protected $validMethods = [
        self::METHOD_GET,
        self::METHOD_PUT,
        self::METHOD_POST,
        self::METHOD_DELETE,
    ];
    protected $apiUrl;
    protected $cURL;

    private $contentKey = 'xmlContent';
    private $username = null;
    private $apikey = null;

    public function __construct($apiUrl, $username = null, $apiKey = null, $contentKey = 'xmlContent')
    {
        $this->contentKey = $contentKey;
        $this->apiUrl = rtrim($apiUrl, '/') . '/';
        //Initializes the cURL instance
        $this->cURL = curl_init();
        curl_setopt($this->cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->cURL, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($this->cURL, CURLOPT_USERAGENT, 'Shopware ApiClient');
        curl_setopt($this->cURL, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        if ($username !== null && strlen($username) > 0) {
            curl_setopt($this->cURL, CURLOPT_USERPWD, $username . ':' . $apiKey);
        }

        $this->username = $username;
        $this->apikey = $apiKey;
    }

    /**
     * @param $url
     * @param string $method
     * @param array $data
     * @param array $params
     * @param string $format
     * @return array
     * @throws Exception
     */
    public function call($url, $method = self::METHOD_GET, $data = [], $params = [], $format = 'json')
    {
        if (!in_array($method, $this->validMethods)) {
            throw new Exception('Invalid HTTP-Methode: ' . $method);
        }
        $queryString = '';
        if (!empty($params)) {
            $queryString = http_build_query($params);
        }
        $url = rtrim($url, '?') . '?';
        $url = $this->apiUrl . $url . $queryString;
        $dataString = $this->contentKey . '=' . urlencode($data[$this->contentKey]);

        if (strtolower($format) == 'json') {
            $dataString = json_encode($data);

            curl_setopt(
                $this->cURL,
                CURLOPT_HTTPHEADER,
                ['Content-Type: application/json; charset=utf-8']
            );
        } else {
            if (strtolower($format) == 'xml') {
                $dataString = $data;

                curl_setopt(
                    $this->cURL,
                    CURLOPT_HTTPHEADER,
                    ['Content-Type: text/xml; charset=utf-8']
                );
            } else {
                if (strtolower($format) == 'format_userpass') {
                    $data['user'] = $this->username;
                    $data['pass'] = $this->apikey;
                } else {
                    if (strtolower($format) == 'format_userpassword') {
                        $data['user'] = $this->username;
                        $data['password'] = $this->apikey;
                    } else {
                        if (strtolower($format) == 'format_userapikey') {
                            $data['user'] = $this->username;
                            $data['apikey'] = $this->apikey;
                        }
                    }
                }
                foreach ($data as $key => $val) {
                    if ($key != $this->contentKey) {
                        $dataString .= '&';
                        $dataString .= $key . '=' . urlencode($val);
                    }
                }
            }
        }

        curl_setopt($this->cURL, CURLOPT_URL, $url);
        curl_setopt($this->cURL, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($this->cURL, CURLOPT_POSTFIELDS, $dataString);
        $result = curl_exec($this->cURL);
        $httpCode = curl_getinfo($this->cURL, CURLINFO_HTTP_CODE);

        return [
            'raw' => $result,
            'code' => $httpCode,
            'json' => preg_match("/^(\[|\{)/i", trim($result)) ? json_decode($result, true) : null,
        ];
    }

    /**
     * @param $url
     * @param array $params
     * @param string $format
     * @return array
     * @throws Exception
     */
    public function get($url, $params = [], $format = 'json')
    {
        return $this->call($url, self::METHOD_GET, [], $params, $format);
    }

    /**
     * @param $url
     * @param array $data
     * @param array $params
     * @param string $format
     * @return array
     * @throws Exception
     */
    public function post($url, $data = [], $params = [], $format = 'json')
    {
        return $this->call($url, self::METHOD_POST, $data, $params, $format);
    }

    /**
     * @param $url
     * @param array $data
     * @param array $params
     * @param string $format
     * @return array
     * @throws Exception
     */
    public function put($url, $data = [], $params = [], $format = 'json')
    {
        return $this->call($url, self::METHOD_PUT, $data, $params, $format);
    }

    /**
     * @param $url
     * @param array $params
     * @param string $format
     * @return array
     * @throws Exception
     */
    public function delete($url, $params = [], $format = 'json')
    {
        return $this->call($url, self::METHOD_DELETE, [], $params, $format);
    }
}
