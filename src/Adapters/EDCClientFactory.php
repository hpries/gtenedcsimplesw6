<?php

namespace GTen\EDCSimple\Adapters;

use Shopware\Core\System\SystemConfig\SystemConfigService;

class EDCClientFactory{

    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    public function __construct(SystemConfigService $systemConfigService)
    {
        $this->systemConfigService = $systemConfigService;
    }

    /**
     * @return EDCClient
     */
    public function getClient(): EDCClient
    {
        $host = $this->systemConfigService->get('GTenEDCSimple.config.edcHost');
        return new EDCClient($host, null, null, 'data');
    }
}
