<?php
namespace GTen\EDCSimple\Commands;

use Exception;
use GTen\EDCSimple\Services\ProductImportService;
use Shopware\Core\Framework\Context;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportProducts extends Command {
    protected static $defaultName = 'edc:products:import';

    /**
     * @var ProductImportService
     */
    private $productImportService;

    public function __construct(ProductImportService $productImportService)
    {
        $this->productImportService = $productImportService;
        parent::__construct(self::$defaultName);
    }

    protected function configure(): void
    {
        $this->setDescription('import XML file with edc products');
        $this->addArgument('xml');
        $this->addOption('offset', null, InputOption::VALUE_OPTIONAL, 'optional offset');
        $this->addOption('limit', null, InputOption::VALUE_OPTIONAL, 'optional limit');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($this->productImportService && file_exists($input->getArgument('xml'))) {
            $data = simplexml_load_string(file_get_contents($input->getArgument('xml')));
            $offset = 0;
            $limit = 0;
            try{
                $offset = $input->getOption('offset') ?? 0;
                $limit = $input->getOption('limit') ?? 0;
            }
            catch(Exception $e) {
                //do nothing
            }

            $products = [];
            if($data->product->count() == 1) {
                $products = [$data->product];
            }
            else {
                $products = $data->product;
            }

            $cnt = 0;
            $onlyAllowedInGerman = true;
            foreach ($products as $productXML) {
                try {
                    if($cnt >= $offset && ($limit == 0 || $cnt < ($offset + $limit))) {
                        $result = $this->productImportService->importByXML($productXML, $onlyAllowedInGerman);
                        if ($result) {
                            $output->writeln(($cnt + 1) . '/' . count($data) . ' import success');
                        } else {
                            $output->writeln(($cnt + 1) . '/' . count($data) . ' import failed');
                        }
                    }
                    else {
                        $output->writeln('skipped (offset: ' . $offset . ', limit: ' . $limit . ')');
                    }

                } catch (Exception $e) {
                    $output->writeln($e->getMessage() . ': ' . $e->getTraceAsString());
                }
                $cnt++;
            }
        } else {
            $output->writeln('service is not loaded or file does not exists');
        }

        // Exit code 0 for success
        return 0;
    }
}