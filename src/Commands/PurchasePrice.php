<?php
namespace GTen\EDCSimple\Commands;

use GTen\EDCSimple\Services\PricesService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PurchasePrice extends Command {
    protected static $defaultName = 'edc:products:purchaseprices';

    /**
     * @var PricesService
     */
    private $service;

    public function __construct(PricesService $service)
    {
        $this->service = $service;
        parent::__construct(self::$defaultName);
    }

    protected function configure(): void
    {
        $this->setDescription('update purchase prices');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->service->updatePurchasePrice();

        // Exit code 0 for success
        return 0;
    }
}