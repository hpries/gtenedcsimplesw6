<?php
namespace GTen\EDCSimple\Commands;

use GTen\EDCSimple\Services\OrderExportService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AutoClose extends Command {
    protected static $defaultName = 'edc:order:autoclose';

    /**
     * @var OrderExportService
     */
    private $exportService;

    public function __construct(OrderExportService $exportService)
    {
        $this->exportService = $exportService;
        parent::__construct(self::$defaultName);
    }

    protected function configure(): void
    {
        $this->setDescription('autoclose orders');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $result = 0;
        try {
            $this->exportService->runClose();
        }
        catch(\Exception $e) {
            $result = 1;
        }

        // Exit code 0 for success
        return $result;
    }
}