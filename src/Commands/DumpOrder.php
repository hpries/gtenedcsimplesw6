<?php
namespace GTen\EDCSimple\Commands;

use GTen\EDCSimple\Services\OrderExportService;
use GTen\EDCSimple\Services\XMLService;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Framework\Context;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DumpOrder extends Command {
    protected static $defaultName = 'edc:order:dump';

    /**
     * @var OrderExportService
     */
    private $exportService;

    /**
     * @var XMLService
     */
    private $xmlService;

    public function __construct(OrderExportService $exportService, XMLService $xmlService)
    {
        $this->exportService = $exportService;
        $this->xmlService = $xmlService;
        parent::__construct(self::$defaultName);
    }

    protected function configure(): void
    {
        $this->setDescription('ordernumber');
        $this->addArgument('orderNumber');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($this->exportService && $this->xmlService && strlen($input->getArgument('orderNumber')) > 0) {
            $number = $input->getArgument('orderNumber');
            $orders = $this->exportService->getOrders($number);
            /**
             * @var $order OrderEntity
             */
            foreach ($orders as $order) {
                $model = $this->exportService->convertOrderToArray($order);
                $model = $this->exportService->decorateModel($model, $order);

                $xml = $this->xmlService->convertModelToXML($model);
                $finalXML = $this->xmlService->transformXML($xml);
                $output->writeln('XML: ' . $finalXML);
            }
        } else {
            $output->writeln('service is not loaded or order-number is missing');
        }

        // Exit code 0 for success
        return 0;
    }
}