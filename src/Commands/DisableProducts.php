<?php
namespace GTen\EDCSimple\Commands;

use GTen\EDCSimple\Services\OrderExportService;
use GTen\EDCSimple\Services\ProductDisableService;
use Shopware\Core\Framework\Context;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DisableProducts extends Command {
    protected static $defaultName = 'edc:products:disable';

    /**
     * @var ProductDisableService
     */
    private $service;

    public function __construct(ProductDisableService $service)
    {
        $this->service = $service;
        parent::__construct(self::$defaultName);
    }

    protected function configure(): void
    {
        $this->setDescription('disable old products');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->service->disableOld();

        // Exit code 0 for success
        return 0;
    }
}