<?php
namespace GTen\EDCSimple\Commands;

use GTen\EDCSimple\Services\InStockService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StockUpdate extends Command {
    protected static $defaultName = 'edc:products:stock';

    /**
     * @var InStockService
     */
    private $service;

    public function __construct(InStockService $service)
    {
        $this->service = $service;
        parent::__construct(self::$defaultName);
    }

    protected function configure(): void
    {
        $this->setDescription('update stock');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->service->run();

        // Exit code 0 for success
        return 0;
    }
}