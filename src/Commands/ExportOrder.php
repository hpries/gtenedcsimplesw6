<?php
namespace GTen\EDCSimple\Commands;

use GTen\EDCSimple\Services\OrderExportService;
use Shopware\Core\Framework\Context;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExportOrder extends Command {
    protected static $defaultName = 'edc:order:export';

    /**
     * @var OrderExportService
     */
    private $exportService;

    public function __construct(OrderExportService $exportService)
    {
        $this->exportService = $exportService;
        parent::__construct(self::$defaultName);
    }

    protected function configure(): void
    {
        $this->setDescription('ordernumber');
        $this->addArgument('orderNumber');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($this->exportService && strlen($input->getArgument('orderNumber')) > 0) {
            $this->exportService->run($input->getArgument('orderNumber'));
        } else {
            $output->writeln('service is not loaded or order-number is missing');
        }

        // Exit code 0 for success
        return 0;
    }
}