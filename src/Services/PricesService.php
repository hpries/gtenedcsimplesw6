<?php
namespace GTen\EDCSimple\Services;



use Exception;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class PricesService {

    private SystemConfigService $systemConfigService;
    private EntityRepository $productRepository;
    private EntityRepository $currencyRepository;

    public function __construct(SystemConfigService $systemConfigService, EntityRepository $productRepository, EntityRepository $currencyRepository)
    {
        $this->systemConfigService = $systemConfigService;
        $this->productRepository = $productRepository;
        $this->currencyRepository = $currencyRepository;
    }

    protected function getContext(): Context
    {
        return Context::createDefaultContext();
    }

    public function loadIdBySupplierNo(string $number): ?string {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('manufacturerNumber', $number));
        return $this->productRepository->searchIds($criteria, $this->getContext())->firstId();
    }

    public function loadBySupplierNo(string $number): ?ProductEntity {
        $criteria = new Criteria();
        $criteria->addAssociation('tax');
        $criteria->addFilter(new EqualsFilter('manufacturerNumber', $number));
        return $this->productRepository->search($criteria, $this->getContext())->first();
    }

    public function loadIdsByParentId(string $id): array {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('parentId', $id));
        return $this->productRepository->searchIds($criteria, $this->getContext())->getIds();
    }

    private function getCurrencyId(string $isoCode): string
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('isoCode', $isoCode));
        return $this->currencyRepository->searchIds($criteria, $this->getContext())->firstId();
    }

    /**
     *
     */
    public function updatePurchasePrice() {
        $file = $this->systemConfigService->get('GTenEDCSimple.config.edcPriceUrl');
        if(!$file) {
            return;
        }

        $stringEnclosure = '"';

        //check for invalid csv-file
        $content = file_get_contents($file);
        if(substr_count('"', $content) % 2 == 1) {
            //$this->logError(new Exception('price-csv contains uneven count of string-enclosures.'));
            $stringEnclosure = '|'; //shouldn't be used here
        }

        $result = [];
        $handle = fopen($file, 'r');
        $header = null;
        while (($data = fgetcsv($handle, 0, ';', $stringEnclosure)) !== false) {
            if ($header === null) {
                foreach ($data as $name) {
                    $header[] = strtoupper(trim($name));
                }
            } else {
                $temp = [];
                foreach ($header as $i => $name) {
                    $temp[$name] = $data[$i];
                }
                $result[] = $temp;
            }
        }

        foreach ($result as $item) {
            try{
                $number = $item['SUBARTNR'] ?? $item['ARTNR'];
                $id = $this->loadIdBySupplierNo($number);
                $data = [];
                if($id) {

                    $product = $this->loadBySupplierNo($number);
                    $taxRate = $product->getTax()->getTaxRate();

                    $purchasePrice = floatval($item['YOUR_PRICE']);
                    $data[] = [
                        'id' => $id,
                        'purchasePrices' => [
                            [
                                'net' => (($purchasePrice / (100 + $taxRate)) * 100),
                                'gross' => $purchasePrice,
                                'linked' => true,
                                'currencyId' => $this->getCurrencyId('EUR'),
                            ]
                        ]
                    ];

                    $ids = $this->loadIdsByParentId($id);
                    foreach ($ids as $child) {
                        $data[] = [
                            'id' => $child,
                            'purchasePrices' => [
                                [
                                    'net' => (($purchasePrice / (100 + $taxRate)) * 100),
                                    'gross' => $purchasePrice,
                                    'linked' => true,
                                    'currencyId' => $this->getCurrencyId('EUR'),
                                ]
                            ]
                        ];
                    }
                }

                if(count($data) > 0) {
                    $this->productRepository->upsert($data, $this->getContext());
                }
            }
            catch(Exception $e) {
                //$this->logError($e);
            }
        }
    }
}