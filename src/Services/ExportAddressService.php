<?php

namespace GTen\EDCSimple\Services;

use Exception;
use Psr\Log\LoggerInterface;

class ExportAddressService
{
    /** @var LoggerInterface */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * for CustomerAddressEntity as array
     * @param array $address
     * @return array
     */
    public function extractHouseNrAndStreetOnly(array $address = []): array
    {
        if (isset($address['street']) && strlen($address['street']) > 0) {
            $address['street'] = trim($address['street']);
            try {
                if (preg_match("/\d+\s\w+$/", $address['street'])) {
                    $address['street'] = preg_replace("/(\d+)\s(\w+)$/", "$1$2", $address['street']);
                }
            } catch (Exception $e) {
                $this->logger->error($e);
            }

            $parts = preg_split("/\s/", $address['street']);
            $street = '';
            $number = '';
            for ($i = 0; $i < count($parts); $i++) {
                if ($i == count($parts) - 1 && preg_match("/\d/", $parts[$i])) {
                    $number .= $parts[$i];
                } else {
                    if (strlen($street) > 0) {
                        $street .= ' ';
                    }
                    $street .= $parts[$i];
                }
            }

            $address['streetSimple'] = $street;
            $address['houseno'] = $number;
            $address['housenoClean'] = trim(preg_replace("/[^0-9]/i", '', $number));
            $address['housenoExt'] = trim(preg_replace("/[^a-zA-Z]/i", '', $number));

            //using improved parsing in accordance to DIN5008
            try{
                $address = array_merge($address, $this->splitStreet($address['street']));
            }
            catch(Exception $eDin) {
                $this->logger->error($eDin);
            }

            if(preg_match("/\d+/i", $address['additionalAddressLine1'] ?? '') && preg_match("/packstation/i", $address['streetSimple'])) {
                $address['dhlid'] = $address['additionalAddressLine1'];
            }

            $address['housenoClean'] = intval($address['housenoClean']);
        }
        return $address;
    }

    /**
     * DIN5008 format
     * @param string $streetStr
     * @return array
     */
    public function splitStreet(string $streetStr): array {
        //clean up the street string
        $streetStr = preg_replace("/\s/", '_', $streetStr);
        $streetStr = preg_replace("/[^a-zA-Z0-9\-\/._]/", '_', $streetStr);
        $streetStr = preg_replace("/_+/", ' ', $streetStr);

        //parse the street string
        $parts = preg_split("/\s/", trim($streetStr));
        $street = '';
        $number = '';

        $numberFound = false;

        $address = [];
        foreach ($parts as $part) {
            if ($numberFound || preg_match("/^\d/", $part)) {
                $number .= $part;
            }
            else {
                if (strlen($street) > 0) {
                    $street .= ' ';
                }
                $street .= $part;
            }
        }

        $address['streetSimple'] = $street;
        $address['houseno'] = $number;

        if(preg_match("/\d+\/\/[a-z0-9]+/", $number)) {
            $parts = preg_split("/(\/)+/", $number);
            $address['housenoClean'] = trim($parts[0]);
            $address['housenoExt'] = trim($parts[1]);
        }
        else if(preg_match("/\d+(\/|\-)\d+/", $number)) {
            $splitted = preg_split("/[\-\/]/i", trim($number));
            $address['housenoClean'] = intval($splitted[0]);
            $address['housenoExt'] = intval($splitted[1]);
        }
        else {
            $address['housenoClean'] = trim(preg_replace("/[^0-9]/i", '', $number));
            $address['housenoExt'] = trim(preg_replace("/[^a-zA-Z]/i", '', $number));
        }

        return $address;
    }
}