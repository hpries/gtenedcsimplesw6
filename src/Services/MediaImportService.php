<?php

namespace GTen\EDCSimple\Services;

use Exception;
use Psr\Log\LoggerInterface;
use Shopware\Core\Content\Media\File\FileSaver;
use Shopware\Core\Content\Media\File\MediaFile;
use Shopware\Core\Content\Media\MediaService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;

/**
 * https://stackoverflow.com/questions/63410548/media-creation-via-php-in-shopware-6
 * removed disableCache in SW 6.4
 */
class MediaImportService
{
    protected MediaService $mediaService;
    private FileSaver $fileSaver;
    private EntityRepository $productMediaRepository;
    private EntityRepository $productRepository;
    private EntityRepository $mediaRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        MediaService              $mediaService,
        FileSaver                 $fileSaver,
        EntityRepository $productMediaRepository,
        EntityRepository $productRepository,
        EntityRepository $mediaRepository,
        LoggerInterface $logger
    )
    {
        $this->mediaService = $mediaService;
        $this->fileSaver = $fileSaver;
        $this->productMediaRepository = $productMediaRepository;
        $this->productRepository = $productRepository;
        $this->mediaRepository = $mediaRepository;
        $this->logger = $logger;
    }

    public function removeAllImages($productId, Context $context)
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('productId', $productId));
        $data = $this->productMediaRepository->search($criteria, $context);
        $ids = [];
        foreach ($data as $media) {
            $ids[] = [
                'id' => $media->getId(),
            ];
        }
        $this->productMediaRepository->delete($ids, $context);
    }

    public function getMD5ByUrl($url): string
    {
        return md5(file_get_contents($url));
    }

    private function getMediaIdByMD5($md5, Context $context): ?string
    {
        $id = null;
        try{
            $criteria = new Criteria();
            $criteria->addFilter(new EqualsFilter('fileName', $md5));
            $id = $this->mediaRepository->searchIds($criteria, $context)->firstId();
        }
        catch(Exception $e) {
            $this->logger->error($e);
        }
        return $id;
    }

    private function isMediaConnected(string $productId, string $mediaId, Context $context): ?string
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('productId', $productId));
        $criteria->addFilter(new EqualsFilter('mediaId', $mediaId));
        return $this->productMediaRepository->searchIds($criteria, $context)->firstId();
    }

    /**
     * @param string $productId
     * @param Context $context
     * @param array $urls
     * @throws Exception
     */
    public function addMediaToProductViaUrls(string $productId, Context $context, array $urls = []): void
    {
        foreach ($urls as $idx => $url) {
            $md5 = $this->getMD5ByUrl($url);

            $mediaId = null;
            if($md5 && $md5 != 'd41d8cd98f00b204e9800998ecf8427e') { //check for 404 errors and empty files
                $mediaId = $this->getMediaIdByMD5($md5, $context);
                if (!$mediaId) {
                    $mediaId = $this->addImageToProductMedia($url, $context, $md5);
                }
            }

            if($mediaId) {
                if (!$this->isMediaConnected($productId, $mediaId, $context) && !($idx === 0)) {
                    $this->productMediaRepository->create([
                        [
                            'productId' => $productId,
                            'mediaId' => $mediaId,
                            'position' => $idx,
                        ]
                    ], $context);
                }

                if ($idx === 0) {
                    $data = [
                        'id' => $productId,
                        'cover' => ['mediaId' => $mediaId],
                    ];
                    $this->productRepository->update([$data], $context);
                }
            }
            else {
                throw new Exception('error on add media (' . $md5 . ')');
            }
        }
    }

    public function addImageToProductMedia($imageUrl, Context $context, string $optionalFilename = null): string
    {
        $mediaId = null;
        try{
            $filePathParts = explode('/', $imageUrl);
            $fileName = array_pop($filePathParts);
            $fileNameParts = explode('.', $fileName);

            $actualFileName = $optionalFilename ?? $fileNameParts[0];
            $fileExtension = $fileNameParts[1] ?? 'jpg';

            if ($actualFileName && $fileExtension) {
                $tempFile = tempnam(sys_get_temp_dir(), 'image-import');
                file_put_contents($tempFile, file_get_contents($imageUrl));

                $fileSize = filesize($tempFile);
                $mimeType = mime_content_type($tempFile);

                $mediaFile = new MediaFile($tempFile, $mimeType, $fileExtension, $fileSize);
                $mediaId = $this->mediaService->createMediaInFolder('product', $context, false);

                $this->fileSaver->persistFileToMedia(
                    $mediaFile,
                    $actualFileName,
                    $mediaId,
                    $context
                );
            }
        }
        catch (Exception $e) {
            $this->logger->error($e);
        }
        return $mediaId;
    }
}