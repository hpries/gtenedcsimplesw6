<?php
namespace GTen\EDCSimple\Services;

use DOMDocument;
use DOMNode;
use DOMXPath;
use Exception;
use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class ProductDisableService {

    private SystemConfigService $systemConfigService;
    private EntityRepository $productRepository;
    private LoggerInterface $logger;

    public function __construct(SystemConfigService $systemConfigService, EntityRepository $productRepository, LoggerInterface $logger)
    {
        $this->systemConfigService = $systemConfigService;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
    }

    protected function getContext(): Context
    {
        return Context::createDefaultContext();
    }

    public function loadIdBySupplierNo($number): ?string {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('manufacturerNumber', $number));
        return $this->productRepository->searchIds($criteria, $this->getContext())->firstId();
    }

    /**
     * @param string $url
     * @return array
     */
    public function loadDisabledProductNumbers(string $url): array {
        $dom = new DOMDocument();
        $dom->loadHTMLFile($url);
        $xpath = new DOMXPath($dom);
        $nodes = $xpath->query("//span[@class='bu_artn']"); //TODO make config-able


        $result = [];
        /** @var DOMNode $node */
        foreach ($nodes as $node) {
            $number = trim(preg_split("/:/", $node->textContent)[1]);
            $result[$number] = $number;
        }
        return $result;
    }

    /**
     * @param array $numbers
     */
    public function disableProductsBySupplierNumber(array $numbers): void {
        foreach ($numbers as $number) {
            if(strlen($number) > 0) {
                try{
                    $productId = $this->loadIdBySupplierNo($number);
                    if(strlen($productId) > 0) {
                        $this->disableProduct($productId);
                    }
                }
                catch(Exception $e) {
                   $this->logError($e);
                }
            }
        }
    }

    /**
     * @param string $id

     */
    public function disableProduct(string $id): void {
        $data = [
            'id' => $id,
            'active' => false,
        ];

        $this->logger->info('get disable notification for ' . $id);

        $catId = $this->systemConfigService->get('GTenEDCSimple.config.edcCategoryDisabled');
        if($catId) {
            $data['categories'] = [
                [
                    'id' => $catId,
                ]
            ];
        }
        $this->productRepository->update([$data], $this->getContext());
    }

    public function disableOld(): void {
        $url = $this->systemConfigService->get('GTenEDCSimple.config.edcDisabledUrl') ?? '';
        if(strlen($url) > 0) {
            $numbers = $this->loadDisabledProductNumbers($url);
            $this->disableProductsBySupplierNumber($numbers);
        }
    }

    public function logError(Exception $e): void
    {
        $this->logger->error($e);
    }
}