<?php

namespace GTen\EDCSimple\Services;

use DateInterval;
use DateTime;
use Exception;
use GTen\EDCSimple\Adapters\EDCClientFactory;
use GTen\EDCSimple\Subscribers\Events\ErrorEDCOrder;
use GTen\EDCSimple\Subscribers\Events\NoneEDCOrder;
use GTen\EDCSimple\Subscribers\Events\SuccessEDCOrder;
use GTen\EDCSimple\Utils\Fields;
use Psr\Log\LoggerInterface;
use Shopware\Core\Checkout\Order\Aggregate\OrderAddress\OrderAddressEntity;
use Shopware\Core\Checkout\Order\OrderDefinition;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Content\Mail\Service\AbstractMailService;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\StateMachine\Aggregation\StateMachineTransition\StateMachineTransitionEntity;
use Shopware\Core\System\StateMachine\StateMachineEntity;
use Shopware\Core\System\StateMachine\StateMachineRegistry;
use Shopware\Core\System\StateMachine\Transition;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class OrderExportService
{
    const MODEL_NONE_EDC_FLAG = 'hasNoneEdcProducts';

    private SystemConfigService $systemConfigService;

    private EntityRepository $orderRepository;
    private EntityRepository $productRepository;
    private XMLService $xmlService;
    private EDCClientFactory $edcClientFactory;
    private ExportAddressService $addressService;
    private LoggerInterface $logger;
    private AbstractMailService $mailService;
    private StateMachineRegistry $stateMachineRegistry;
    private EntityRepository $stateRepository;
    private EntityRepository $stateTransitionRepository;
    private EventDispatcherInterface $events;

    public function __construct(
        SystemConfigService $systemConfigService,
        EntityRepository $orderRepository,
        EntityRepository $productRepository,
        XMLService $xmlService,
        ExportAddressService $addressService,
        EDCClientFactory $factory,
        LoggerInterface $logger,
        AbstractMailService $mailService,
        StateMachineRegistry $stateMachineRegistry,
        EntityRepository $stateRepository,
        EntityRepository $stateTransitionRepository,
        EventDispatcherInterface $events
    ) {
        $this->systemConfigService = $systemConfigService;
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
        $this->xmlService = $xmlService;
        $this->edcClientFactory = $factory;
        $this->addressService = $addressService;
        $this->logger = $logger;
        $this->mailService = $mailService;
        $this->stateMachineRegistry = $stateMachineRegistry;
        $this->stateRepository = $stateRepository;
        $this->stateTransitionRepository = $stateTransitionRepository;
        $this->events = $events;
    }

    protected function getContext(): Context
    {
        return Context::createDefaultContext();
    }

    public function getOrders(string $orderNumber = null, array $stateIds = [], array $paymentStateIds = []): EntitySearchResult
    {
        if(count($stateIds) === 0) {
            $stateIds = $this->systemConfigService->get('GTenEDCSimple.config.edcOrderQueryState');
        }
        if(count($paymentStateIds) === 0) {
            $paymentStateIds = $this->systemConfigService->get('GTenEDCSimple.config.edcOrderQueryPaymentState');
        }

        $criteria = new Criteria();
        $criteria->addAssociation('lineItems')
            ->addAssociation('lineItems.product')
            ->addAssociation('orderCustomer')
            ->addAssociation('addresses')
            ->addAssociation('deliveries')
            ->addAssociation('billingAddress')
            ->addAssociation('addresses')
            ->addAssociation('addresses.country')
            ->addAssociation('transactions');

        if($orderNumber) {
            $criteria->addFilter(new EqualsFilter('orderNumber', $orderNumber));
        }
        else {
            $criteria->addFilter(new EqualsAnyFilter('stateId', $stateIds));

            if(count($paymentStateIds) > 0) {
                $criteria->addFilter(new EqualsAnyFilter('transactions.stateId', $paymentStateIds));
            }
        }

        $criteria->setLimit(100); //if more you need more runs to export all orders

        return $this->orderRepository->search($criteria, $this->getContext());
    }

    private function getTransitionName(string $fromId, string $toId): ?string {
        $result = null;
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('fromStateId', $fromId));
        $criteria->addFilter(new EqualsFilter('toStateId', $toId));

        /** @var StateMachineTransitionEntity $transition */
        $transition = $this->stateTransitionRepository->search($criteria, $this->getContext())->first();
        if($transition) {
            $result = $transition->getActionName();
        }
        return $result;
    }

    private function changeOrderState(string $orderId, string $stateId)
    {
        $criteria = new Criteria([$orderId]);

        /** @var OrderEntity $order */
        $order = $this->orderRepository->search($criteria, $this->getContext())->first();

        if ($order && $order->getStateId() != $stateId) {
            $transition = $this->getTransitionName($order->getStateId(), $stateId);

            if($transition) {
                $this->stateMachineRegistry->transition(new Transition(
                    OrderDefinition::ENTITY_NAME,
                    $order->getId(),
                    $transition,
                    'stateId'
                ), $this->getContext());
            }
        }
    }

    private function setOrderSuccessState(OrderEntity $order, bool $noneEdc = false)
    {
        $this->events->dispatch(
            $noneEdc ?
                new SuccessEDCOrder($order, $this->getContext()) :
                new NoneEDCOrder($order, $this->getContext())
        );
        $stateId = $this->systemConfigService->get('GTenEDCSimple.config.edcOrderExportedState');
        $this->changeOrderState($order->getId(), $stateId);
    }

    private function setOrderErrorState(OrderEntity $order, array $response = [])
    {
        $this->events->dispatch(
            new ErrorEDCOrder($order, $this->getContext(), $response)
        );
        $stateId = $this->systemConfigService->get('GTenEDCSimple.config.edcOrderErrorState');
        $this->changeOrderState($order->getId(), $stateId);
    }

    private function setOrderCloseState(string $orderId)
    {
        $stateId = $this->systemConfigService->get('GTenEDCSimple.config.closeStateIdTo');
        $this->changeOrderState($orderId, $stateId);
    }

    /**
     * @param array $model
     * @param OrderEntity $order
     * @return array
     */
    public function decorateModel(array $model, OrderEntity $order): array
    {
        foreach ($model['lineItems'] as $idx => $item) {
            //list quantity as separate numbers list
            $plainNumberList = [];
            for ($i = 0; $i < $item['quantity']; $i++) {
                $plainNumberList[] = ['number' => $item['supplierNumber']];
            }
            $model['lineItems'][$idx]['purchaseInfo']['supplierNumberQuantityLists'] = $plainNumberList;
        }

        //add edc/connection/user + apikey
        $model['edc'] = [
            'connection' => [
                'user' => $this->systemConfigService->get('GTenEDCSimple.config.edcuser'),
                'apikey' => $this->systemConfigService->get('GTenEDCSimple.config.edcapikey'),
            ]
        ];

        $model['shippingAddress'] = $this->addressService->extractHouseNrAndStreetOnly($model['shippingAddress']);

        return $model;
    }

    private function hasValidConfig(): bool
    {
        return strlen($this->systemConfigService->get('GTenEDCSimple.config.edcuser')) > 0
            && strlen($this->systemConfigService->get('GTenEDCSimple.config.edcapikey')) > 0;
    }

    private function getShippingAddress(OrderEntity $order): OrderAddressEntity {
        $result = null;
        foreach($order->getAddresses() as $addr) {
            if($addr->getId() != $order->getBillingAddress()->getId()) {
                $result = $addr;
            }
        }
        return $result ?? $order->getBillingAddress();
    }

    public function convertOrderToArray(OrderEntity $order, array $model = []): array {
        $shippingAddress = $this->getShippingAddress($order);
        
        $model['orderNumber'] = $order->getOrderNumber();
        $model['lineItems'] = [];
        $model['shippingAddress'] = [
            'firstName' => $shippingAddress->getFirstName(),
            'lastName' => $shippingAddress->getLastName(),
            'street' => $shippingAddress->getStreet(),
            'zipCode' => $shippingAddress->getZipcode(),
            'city' => $shippingAddress->getCity(),
            'country' => [
                'iso' => $shippingAddress->getCountry()->getIso(),
            ],
            'phone' => $shippingAddress->getPhoneNumber(),
        ];
        $model['customer'] = [
            'email' => $order->getOrderCustomer()->getEmail(),
        ];
        $model[self::MODEL_NONE_EDC_FLAG] = false;

        foreach ($order->getLineItems() as $lineItem) {
            if($lineItem->getReferencedId()) {
                $criteria = new Criteria([$lineItem->getReferencedId()]);
                /** @var ProductEntity $product */
                $product = $this->productRepository->search($criteria, $this->getContext())->first();
                $isNoneEDCProduct = isset($product->getCustomFields()[Fields::PRODUCT_NONE_EDC]) && $product->getCustomFields()[Fields::PRODUCT_NONE_EDC];

                if(!$isNoneEDCProduct) {
                    $model['lineItems'][] = [
                        'quantity' => $lineItem->getQuantity(),
                        'productId' => $lineItem->getReferencedId(),
                        'supplierNumber' => $lineItem->getProduct()->getManufacturerNumber(),
                        'purchaseInfo' => [
                            'ean' => $lineItem->getProduct()->getEan(),
                            'supplierNumber' => $lineItem->getProduct()->getManufacturerNumber(),
                        ]
                    ];
                }
                else {
                    $model[self::MODEL_NONE_EDC_FLAG] = true;
                }
            }
        }

        return $model;
    }

    /**
     * @param string|null $orderNumber
     * @return array
     * @throws Exception
     */
    public function run(string $orderNumber = null): array
    {
        if (!$this->hasValidConfig()) {
            throw new Exception('no valid EDC configuration [user/apikey]');
        }

        $results = [];

        $client = $this->edcClientFactory->getClient();
        $path = $this->systemConfigService->get('GTenEDCSimple.config.edcOrderPath');
        $orders = $this->getOrders($orderNumber);

        /**
         * @var $order OrderEntity
         */
        foreach ($orders as $order) {
            $model = $this->convertOrderToArray($order);
            $model = $this->decorateModel($model, $order);

            $xml = $this->xmlService->convertModelToXML($model);
            $finalXML = $this->xmlService->transformXML($xml);

            $result = ['not_performed' => true];
            $id = $model['id'];

            if($model[self::MODEL_NONE_EDC_FLAG]) {
                $this->setOrderSuccessState($order, true);
            }

            if (preg_match("/artnr/", $finalXML)) {
                $result = $client->post($path, ['data' => $finalXML], [], 'form');
                if ($result['json']['result'] == 'FAIL') {
                    $this->logger->error($result['message']);
                    $this->setOrderErrorState($order, $result);
                    throw new Exception('EDC export failed: ' . $result['message']);
                } else {
                    $this->setOrderSuccessState($order);
                }
            }

            $results[$id] = $result;
        }

        return $results;
    }

    /**
     * @return void
     * @throws Exception
     */
    public function runClose(): void {
        $stateIds = $this->systemConfigService->get('GTenEDCSimple.config.closeStateIdsFrom');
        if(count($stateIds) > 0){
            $orders = $this->getOrders(null, $stateIds);
            $days = $this->systemConfigService->get('GTenEDCSimple.config.closeDays') ?? 3;
            $checkDate = (new DateTime())->sub(new DateInterval('P' . $days . 'D'));

            /** @var OrderEntity $order */
            foreach ($orders as $order) {
                if($checkDate->getTimestamp() - $order->getOrderDateTime()->getTimestamp() > 0) {
                    $this->setOrderCloseState($order->getId());
                }
            }
        }
    }

    public function logError(Exception $e)
    {
        $this->logger->error($e);
    }
}