<?php

namespace GTen\EDCSimple\Services;

use Exception;
use Psr\Log\LoggerInterface;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class TrackingNumberService
{
    private SystemConfigService $systemConfigService;
    private EntityRepository $orderRepository;
    private EntityRepository $deliveryRepository;
    private LoggerInterface $logger;

    public function __construct(
        SystemConfigService $systemConfigService,
        EntityRepository $orderRepository,
        EntityRepository $deliveryRepository,
        LoggerInterface $logger
    ) {
        $this->systemConfigService = $systemConfigService;
        $this->orderRepository = $orderRepository;
        $this->deliveryRepository = $deliveryRepository;
        $this->logger = $logger;
    }

    protected function getContext(): Context
    {
        return Context::createDefaultContext();
    }

    public function updateOrder(string $orderNumber, string $trackingNumber): void
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('orderNumber', $orderNumber));
        $criteria->addAssociation('deliveries');
        $criteria->addAssociation('deliveries.trackingcodes');

        /**
         * @var $order OrderEntity
         */
        $order = $this->orderRepository->search($criteria, $this->getContext())->first();
        if ($order) {
            $deliveries = $order->getDeliveries();
            foreach ($deliveries as $delivery) {
                if (count($delivery->getTrackingCodes()) === 0) {
                    $trackingNumbers = $delivery->getTrackingCodes() ?? [];
                    if(!in_array($trackingNumber, $trackingNumbers)) {
                        $trackingNumbers[] = $trackingNumber;

                        $this->deliveryRepository->update([[
                            'id' => $delivery->getId(),
                            'trackingCodes' => $trackingNumbers,
                        ]], $this->getContext());

                        //TODO set order state
                    }
                }
            }
        }
    }

    public function logError(Exception $e): void
    {
        $this->logger->error($e);
    }
}