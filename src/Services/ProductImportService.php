<?php

namespace GTen\EDCSimple\Services;

use Exception;
use GTen\EDCSimple\Utils\Fields;
use Psr\Log\LoggerInterface;
use Shopware\Core\Content\Product\Aggregate\ProductManufacturer\ProductManufacturerEntity;
use Shopware\Core\Content\Product\Aggregate\ProductVisibility\ProductVisibilityDefinition;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Property\Aggregate\PropertyGroupOption\PropertyGroupOptionEntity;
use Shopware\Core\Content\Property\PropertyGroupEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Core\System\Tax\TaxEntity;

class ProductImportService
{
    private $imagesBaseUrl = 'http://cdn.edc-internet.nl/';
    const VARIANT_OPTIONGROUP = 'Variant';

    private EntityRepository $productRepository;
    private EntityRepository $manufacturerRepository;
    private EntityRepository $taxRepository;
    private EntityRepository $currencyRepository;
    private EntityRepository $propertyGroupRepository;
    private EntityRepository $propertyGroupOptionRepository;
    private EntityRepository $productPropertyRepository;
    private LoggerInterface $logger;
    private MediaImportService $mediaImportService;
    private SystemConfigService $systemConfigService;
    private CategoryService $categoryService;
    private EntityRepository $salesChannelsRepository;
    private EntityRepository $productConfigRepository;

    public function __construct(
        EntityRepository $productRepository,
        EntityRepository $productManufacturerRepository,
        EntityRepository $taxRepository,
        EntityRepository $currencyRepository,
        EntityRepository $propertyGroupRepository,
        EntityRepository $propertyGroupOptionRepository,
        EntityRepository $productPropertyRepository,
        LoggerInterface $logger,
        MediaImportService $mediaImportService,
        SystemConfigService $systemConfigService,
        CategoryService $categoryService,
        EntityRepository $salesChannelsRepository,
        EntityRepository $productConfigRepository
    ) {
        $this->productRepository = $productRepository;
        $this->manufacturerRepository = $productManufacturerRepository;
        $this->taxRepository = $taxRepository;
        $this->currencyRepository = $currencyRepository;
        $this->propertyGroupRepository = $propertyGroupRepository;
        $this->propertyGroupOptionRepository = $propertyGroupOptionRepository;
        $this->productPropertyRepository = $productPropertyRepository;
        $this->logger = $logger;
        $this->mediaImportService = $mediaImportService;
        $this->systemConfigService = $systemConfigService;
        $this->categoryService = $categoryService;
        $this->salesChannelsRepository = $salesChannelsRepository;
        $this->productConfigRepository = $productConfigRepository;
    }

    protected function getContext(): Context
    {
        return Context::createDefaultContext();
    }

    private function getProductIdByNumber(string $number): ?string
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('manufacturerNumber', $number));

        /**
         * @var $product ProductEntity
         */
        $product = $this->productRepository->search($criteria, $this->getContext())->first();
        return $product ? $product->getId() : null;
    }

    private function getManufacturerIdByName($name): string
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('name', $name));

        /**
         * @var $manufacturer ProductManufacturerEntity
         */
        $manufacturer = $this->manufacturerRepository->search($criteria, $this->getContext())->first();
        if (!$manufacturer) {
            $this->manufacturerRepository->create(
                [[
                    'name' => $name,
                ]],
                $this->getContext()
            );
            $manufacturer = $this->manufacturerRepository->search($criteria, $this->getContext())->first();
        }
        return $manufacturer->getId();
    }

    private function getTaxIdByValue(float $value = 19.00): string
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('taxRate', $value));

        /**
         * @var $tax TaxEntity
         */
        $tax = $this->taxRepository->search($criteria, $this->getContext())->first();
        return $tax->getId();
    }

    private function getCurrencyId(string $isoCode): string
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('isoCode', $isoCode));
        return $this->currencyRepository->searchIds($criteria, $this->getContext())->firstId();
    }

    private function getEURCurrencyId(): ?string
    {
        return $this->getCurrencyId('EUR');
    }

    private function getInsertPrice(float $grossPrice, float $raxRate = 19.00): array
    {
        $currencyId = $this->getEURCurrencyId();

        return [
            [
                'net' => (($grossPrice / (100 + $raxRate)) * 100),
                'gross' => $grossPrice,
                'linked' => false,
                'currencyId' => $currencyId,
            ]
        ];
    }

    private function getPropertyGroupIdByName(string $name): string
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('name', $name));

        /**
         * @var $group PropertyGroupEntity
         */
        $group = $this->propertyGroupRepository->search($criteria, $this->getContext())->first();
        if (!$group) {
            $this->propertyGroupRepository->create(
                [
                    [
                        'name' => $name,
                        'filterable' => $name != 'Variant',
                    ]
                ],
                $this->getContext()
            );
            $group = $this->propertyGroupRepository->search($criteria, $this->getContext())->first();
        }
        return $group->getId();
    }

    private function getOptionIdByNameAndGroup(string $name, string $groupId): string
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('name', $name));
        $criteria->addFilter(new EqualsFilter('groupId', $groupId));

        /**
         * @var $option PropertyGroupOptionEntity
         */
        $option = $this->propertyGroupOptionRepository->search($criteria, $this->getContext())->first();
        if (!$option) {
            $this->propertyGroupOptionRepository->create(
                [
                    [
                        'name' => $name,
                        'groupId' => $groupId,
                    ]
                ],
                $this->getContext()
            );
            $option = $this->propertyGroupOptionRepository->search($criteria, $this->getContext())->first();
        }
        return $option->getId();
    }

    private function addPropertyOption(string $productId, string $optionId): void
    {
        $this->productPropertyRepository->create(
            [
                [
                    'productId' => $productId,
                    'optionId' => $optionId,
                ]
            ],
            $this->getContext()
        );
    }

    private function addProperties(string $productId, array $properties = []): void
    {
        foreach ($properties as $groupName => $option) {
            if($option && strlen($option) > 0) {
                $groupId = $this->getPropertyGroupIdByName($groupName);
                $optionId = $this->getOptionIdByNameAndGroup($option, $groupId);

                $this->addPropertyOption($productId, $optionId);
            }
        }
    }

    public function logError(Exception $e): void
    {
        var_dump($e->getMessage() . ' (' . $e->getTraceAsString() . ')');
        $this->logger->error($e);
    }

    private function getConfig($key) {
        return $this->systemConfigService->get('GTenEDCSimple.config.' . $key);
    }

    private function getBaseImageUrl(): string {
        $quality = $this->getConfig('edcImageQuality') ?? '500';
        return $this->imagesBaseUrl . $quality . '/';
    }

    private function makeVariant(string $main, string $variant, string $optionId, bool $isMain = false): void {
        $data = [[
            'id' => $this->getProductIdByNumber($variant),
            'parentId' => $this->getProductIdByNumber($main),
            'options' => [['id' => $optionId]],
        ]];

        //if current the same number as the main but without the .m ...set to parent the variant config
        if($isMain || $main == $variant . '.m') {
            $data[] = [
                'id' => $this->getProductIdByNumber($main),
                'variantListingConfig' => [
                    'displayParent' => true,
                    'mainVariantId' => $this->getProductIdByNumber($variant),
                ]
            ];
        }

        $this->productRepository->update($data, $this->getContext());

        //add configurator, needed to show the variant selection in product-detail
        $configData = [
            'productId' => $this->getProductIdByNumber($main),
            'optionId' => $optionId,
            'position' => 0,
        ];

        $this->productConfigRepository->upsert([$configData], $this->getContext());
    }

    private function getVisibleToAllSalesChannels():array {
        $result = [];
        $criteria = new Criteria();
        $ids = $this->salesChannelsRepository->searchIds($criteria, $this->getContext())->getIds();
        foreach ($ids as $id) {
            $result[] = [
                'salesChannelId' => $id,
                'visibility' => ProductVisibilityDefinition::VISIBILITY_ALL,
            ];
        }

        return $result;
    }

    /**
     * @param $productXML
     * @param bool $onlyAllowedInGermany
     * @return bool
     */
    public function importByXML($productXML, $onlyAllowedInGermany = true): bool {
        $result = false;
        $variants = [];
        if($productXML->variants->variant->count() == 0) {
            $variants = [$productXML->variants->variant];
        }
        else {
            for ($i = 0; $i < $productXML->variants->variant->count(); $i++) {
                $variants[] = $productXML->variants->variant[$i];
            }
        }

        //create main-product
        if(count($variants) > 1) {
            $main = clone $variants[0];
            $main->title = null;
            $main->subartnr = $main->subartnr . '.m';
            $variants = array_merge([$main], $variants);
        }

        foreach ($variants as $variant) {
            $mode = $this->getConfig('edcImportMode') ?? 'insert';

            try {
                if ($onlyAllowedInGermany && $productXML->restrictions->germany == 'Y') {
                    return $result; //break because not allowed in germany but only import allowed items
                }

                //article
                $id = $this->getProductIdByNumber((string) $variant->subartnr);
                $isVariant = !preg_match("/\.m$/i", $variant->subartnr) && count($variants) > 1;
                $isMainVariant = $isVariant && $productXML->artnr == $variant->subartnr;

                $isActive = ($this->getConfig('edcimportasactive') ?? 'yes') == 'yes' || ($this->getConfig('edcimportasactive') == 'not_variants' && !$isVariant);

                if ($mode === 'insert_only' && $id) {
                    var_dump('exists: ' . $variant->subartnr . ' -> ' . $id);
                    return true; //already exists, no update allowed
                }

                $isNew = $id ? false : true;

                $subtitle =  (isset($variant->title) && strlen($variant->title) > 0 ? ' (' . $variant->title . ')' : '');
                $variantTitle =  (isset($variant->title) && strlen($variant->title) > 0 ? $variant->title : '');
                $data = [
                    'name' => $productXML->title . $subtitle,
                    'description' => preg_replace("/\n/", '<br/>', $productXML->description),
                    'manufacturer' => ['id' => $this->getManufacturerIdByName((string) $productXML->brand->title), 'name' => ((string) $productXML->brand->title)],
                    'ean' => (string) $variant->ean,
                    'weight' => (float) ($productXML->measures->weight ?? 0),
                    'customFields' => [
                        Fields::PRODUCT_NONE_EDC => false,
                    ]
                ];

                if(substr_count($productXML->measures->packing ?? '','x') == 2){
                    $partsPackaging =  preg_split("/x/i", $productXML->measures->packing);

                    $data['length'] = $partsPackaging[0];
                    $data['width'] = $partsPackaging[1];
                    $data['height'] = $partsPackaging[2];
                }

                if($id) {
                    $data['id'] = $id;
                    if($mode != 'upsert_noprice') {
                        $data['price'] = $this->getInsertPrice((float) $productXML->price->b2c, floatval($productXML->price->vatde));
                    }
                }
                else {
                    $data['active'] = $isActive;
                    $data['manufacturerNumber'] = (string) $variant->subartnr;
                    $data['isCloseout'] = true;
                    $data['stock'] = 0;
                    $data['price'] = $this->getInsertPrice((float) $productXML->price->b2c, floatval($productXML->price->vatde));
                    $data['taxId'] = $this->getTaxIdByValue(floatval($productXML->price->vatde));
                    $data['visibilities'] = $this->getVisibleToAllSalesChannels();
                    $data['categories'] = []; //fill later

                    if($this->getConfig('edcSupplierNumber')) {
                        $data['productNumber'] = (string) (strlen($variant->subartnr) ? $variant->subartnr : $productXML->artnr);
                    }
                    else {
                        $data['productNumber'] = (string) (strlen($variant->subartnr) ? $variant->subartnr : $productXML->artnr); //TODO implement getNextNumber()
                    }
                }

                //feature set
                $featureSetId = $this->getConfig('edcDefaultFeatureSet') ?? null;
                if($featureSetId) {
                    $data['featureSet'] = [
                        'id' => $featureSetId,
                    ];
                }

                //categories
                $importCategories = $this->getConfig('edcimportcategories') == 'yes';
                if($importCategories) {
                    try {
                        $catsXML = [];
                        $cats = [];
                        //new-style
                        foreach($productXML->new_categories->category->cats as $catXML) {
                            $catsXML[] = (string) $catXML->title;
                        }

                        if($this->getConfig('edccategoryroot')) {
                            $cats[] = $this->categoryService->buildCategoryTree($catsXML, $this->getConfig('edccategoryroot'));
                        }

                        if(is_array($cats) && count($cats) > 0) {
                            foreach ($cats as $cat) {
                                $data['categories'][] = ['id' => $cat];
                            }
                        }
                    }
                    catch(Exception $eCats) {
                        $this->logError($eCats);
                    }
                }
                else if($this->getConfig('edcimportcategories') == 'default' && strlen($this->getConfig('edccategoryimport')) > 0 && !$id) {
                    $data['categories'][] = ['id' => $this->getConfig('edccategoryimport')];
                }
                else if($this->getConfig('edcimportcategories') == 'mapping' && !$id) {
                    $catsXML = [];
                    //new-style
                    foreach($productXML->new_categories->category as $catXML) {
                        $catsXML[] = (string) $catXML->id;
                    }

                    if(count($catsXML) > 0) {
                        $catIds = $this->categoryService->getIdsByExternalId($catsXML[count($catsXML) - 1]);
                        if(count($catIds) > 0){
                            foreach ($catIds as $catId) {
                                $data['categories'][] = ['id' => $catId];
                            }
                        }
                    }
                }
                else {
                    $this->logger->warning('no category association defined on import for edc-article ' . $variant->subartnr . ' (please check your plugin-config!)');
                }

                $this->productRepository->upsert([$data], $this->getContext());
                //------------
                $id = $this->getProductIdByNumber((string) $variant->subartnr);

                // properties
                $properties = [
                    'Material' => $productXML->material ?? null,
                    'Batterie benötigt' => ($productXML->battery->required ?? 'N') == 'N' ? 'nein' : 'ja',
                ];

                if($isVariant && strlen($variantTitle) > 0) {
                    $properties[self::VARIANT_OPTIONGROUP] = $variantTitle;
                }
                foreach ($productXML->properties->prop as $prop) {
                    $unit = $prop->values->value[0]->unit ?? null;
                    if($unit) {
                        $properties[(string) $prop->property] = $prop->value . ' ' . $unit;
                    }
                    else {
                        $properties[(string) $prop->property] = (string) $prop->value;
                    }
                }
                $this->logger->info(count($properties) . 'x properties for edc-article ' . $variant->subartnr);
                $this->addProperties($id, $properties);

                //variant
                if(isset($properties[self::VARIANT_OPTIONGROUP]) && $isNew) {
                    $groupId = $this->getPropertyGroupIdByName(self::VARIANT_OPTIONGROUP);
                    $optionId = $this->getOptionIdByNameAndGroup($variantTitle, $groupId);
                    $this->makeVariant($productXML->artnr . '.m', $variant->subartnr, $optionId, $isMainVariant);
                }

                // images
                try {
                    $images = [];
                    $pics = [];
                    if($productXML->pics->pic->count() == 0) {
                        $pics = [$productXML->pics->pic];
                    }
                    else {
                        $pics = $productXML->pics->pic;
                    }

                    foreach ($pics as $pic) {
                        if(preg_match("/^http/i", $pic)) {
                            $images[] = $pic;
                        }
                        else {
                            $images[] = $this->getBaseImageUrl() . $pic;
                        }
                    }

                    if (count($images) > 0) {
                        $this->mediaImportService->addMediaToProductViaUrls($id, $this->getContext(), $images);
                    }
                    else {
                        $this->logger->warning('no images for edc-article ' . $variant->subartnr);
                    }
                } catch (Exception $eMedia) {
                    $this->logError($eMedia);
                }

                // bullet points
                if($productXML->bulletpoints->bp->count() > 0) {
                    $bulletPoints = '<ul>';
                    foreach ($productXML->bulletpoints->bp as $bulletpoint) {
                        $bulletPoints .= '<li>' . $bulletpoint . '</li>';
                    }
                    $bulletPoints .= '</ul>';
                    try{
                        $bp = [
                            'id' => $id,
                            'customFields' => [
                                Fields::PRODUCT_BULLETPOINTS => $bulletPoints,
                            ],
                        ];
                        $this->productRepository->upsert([$bp], $this->getContext());
                    }
                    catch (Exception $eBP) {
                        $this->logError($eBP);
                    }
                }

                $result = true;
            } catch (Exception $e) {
                $this->logError($e);
            }
        }
        return $result;
    }
}