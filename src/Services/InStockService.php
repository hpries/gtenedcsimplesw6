<?php

namespace GTen\EDCSimple\Services;

use DOMDocument;
use DOMNode;
use Exception;
use Psr\Log\LoggerInterface;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class InStockService
{
    private SystemConfigService $systemConfigService;
    private EntityRepository $productRepository;
    private LoggerInterface $logger;

    public function __construct(
        SystemConfigService $systemConfigService,
        EntityRepository $productRepository,
        LoggerInterface $logger
    ) {
        $this->systemConfigService = $systemConfigService;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
    }

    protected function getContext(): Context
    {
        return Context::createDefaultContext();
    }

    private function updateProduct($productNumber, $stock = 0)
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('manufacturerNumber', $productNumber));

        /**
         * @var $product ProductEntity
         */
        $product = $this->productRepository->search($criteria, $this->getContext())->first();
        if ($product) {
            $this->productRepository->update([
                [
                    'id' => $product->getId(),
                    'stock' => $stock,
                ]
            ], $this->getContext());
        }
    }

    public function run()
    {
        $url = $this->systemConfigService->get('GTenEDCSimple.config.edcInStockUrl');

        $xml = new DOMDocument();
        $content = file_get_contents($url);
        $xml->loadXML($content);
        $products = $xml->getElementsByTagName('product');
        foreach ($products as $product) {
            $extProductNumber = '';
            $inStock = 0;

            /** @var DOMNode $child */
            foreach ($product->childNodes as $child) {
                if (strtolower($child->nodeName) == 'productnr') {
                    $extProductNumber = trim($child->nodeValue);
                } else {
                    if (strtolower($child->nodeName) == 'qty') {
                        $inStock = intval(trim($child->nodeValue));
                    }
                }
            }

            if (strlen($extProductNumber) > 0) {
                try {
                    $this->updateProduct($extProductNumber, $inStock);
                } catch (Exception $e) {
                    $this->logError($e);
                }
            }
        }
    }

    public function logError(Exception $e)
    {
        $this->logger->error($e);
    }
}