<?php
namespace GTen\EDCSimple\Services;

use GTen\EDCSimple\Utils\Fields;
use Shopware\Core\Content\Category\CategoryEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;

class CategoryService {

    /**
     * @var EntityRepository
     */
    private EntityRepository $categoryRepository;

    public function __construct(EntityRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    protected function getContext(): Context
    {
        return Context::createDefaultContext();
    }

    private function getLevelById($id)
    {
        $criteria = new Criteria([$id]);
        /** @var CategoryEntity $cat */
        $cat = $this->categoryRepository->search($criteria, $this->getContext())->first();
        return $cat->getLevel();
    }

    private function getIdByName($name)
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('name', $name));
        return $this->categoryRepository->searchIds($criteria, $this->getContext())->firstId();
    }

    private function createCategory($cat, $parentId): string
    {
        $data = [
            'name' => $cat,
            'level' => $this->getLevelById($parentId) + 1,
            'visible' => true,
            'displayNestedProducts' => true,
            'type' => 'page',
            'parentId' => $parentId,
        ];

        $this->categoryRepository->create([$data], $this->getContext());
        //no need to refresh breadcrumbs

        return $this->getIdByName($cat);
    }

    /**
     * @param string[] $cats
     * @param null|string $parentId
     * @return string|null
     */
    public function buildCategoryTree(array $cats = [], ?string $parentId = null): ?string
    {
        if (count($cats) > 0) {
            /** @var string $cat */
            $cat = array_shift($cats);
            $id = $this->getIdByName($cat);
            if (!$id) {
                $id = $this->createCategory($cat, $parentId);
            }
            return $this->buildCategoryTree($cats, $id);
        } else {
            return $parentId;
        }
    }

    public function getIdsByExternalId($extId) {
        $result = [];
        $criteria = new Criteria();
        $cats = $this->categoryRepository->search($criteria, $this->getContext());
        /** @var CategoryEntity $cat */
        foreach ($cats as $cat) {
            $extIds = preg_split("/[.,;]/i", $cat->getCustomFields()[Fields::CATEGORY_EDCID] ?? '');
            if(in_array($extId, $extIds)) {
                $result[] = $cat->getId();
            }
        }
        return $result;
    }
}