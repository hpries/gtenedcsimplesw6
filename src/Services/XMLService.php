<?php

namespace GTen\EDCSimple\Services;

use DateTime;
use DOMDocument;
use DOMException;
use DOMNode;
use Exception;
use Psr\Log\LoggerInterface;
use XSLTProcessor;

class XMLService
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param array $array
     * @return false|string
     * @throws DOMException
     */
    public function convertModelToXML(array $array = []): bool|string
    {
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = true;
        $root = $dom->createElement('order');
        $dom->appendChild($root);
        $holdNullAndEmpty = false;

        $array2xml = function (DOMNode $node, $array) use ($dom, &$array2xml, $holdNullAndEmpty) {
            foreach ($array as $key => $value) {
                if ($value instanceof DateTime) {
                    $value = $value->format('Y-m-d\TH:i:s');
                }

                if (((is_array($value) && count($value) > 0) || (!is_object($value) && !is_array($value) && (strlen($value) > 0 || $holdNullAndEmpty))) && !is_array($key)) {
                    $key = preg_replace("/[^a-zA-Z0-9]/i", '', $key);
                    if (preg_match("/^\d+$/", $key)) {
                        if (preg_match("/s$/", $node->nodeName)) {
                            $key = preg_replace("/s$/", '', $node->nodeName);
                        } else {
                            $key = 'e' . $key;
                        }
                    }
                    if (is_array($value)) {
                        $n = $dom->createElement($key);
                        $node->appendChild($n);
                        $array2xml($n, $value);
                    } else {
                        $attr = $dom->createElement($key);
                        $value = preg_replace("/[&]/", '&amp;', $value);
                        $attr->nodeValue = preg_replace("/[<>]/", " ", $value);
                        $node->appendChild($attr);
                    }
                }
            }
        };

        $array2xml($root, $array);
        return $dom->saveXML();
    }

    public function transformXML(string $xml): ?string
    {
        $result = $xml;
        $xsltPath = __DIR__ . '/../Resources/xslt/edc.xslt';
        try {
            if (is_file($xsltPath)) {
                $xmlDom = new DOMDocument();
                $xmlDom->loadXML($xml);

                $xslt = new DOMDocument();
                $xslt->load($xsltPath);

                $processor = new XSLTProcessor();
                $processor->importStylesheet($xslt);

                $result = $processor->transformToXml($xmlDom);
            }
        } catch (Exception $e) {
            $this->logger->error($e);
        }
        return $result;
    }
}