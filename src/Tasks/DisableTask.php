<?php
namespace GTen\EDCSimple\Tasks;

use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;

class DisableTask extends ScheduledTask {
    public static function getTaskName(): string
    {
        return self::class;
    }

    public static function getDefaultInterval(): int
    {
        return 3600; // 120 minutes
    }
}