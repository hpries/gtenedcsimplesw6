<?php
namespace GTen\EDCSimple\Tasks;

use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;

class OrderExportTask extends ScheduledTask {
    public static function getTaskName(): string
    {
        return self::class;
    }

    public static function getDefaultInterval(): int
    {
        return 300; // 5 minutes
    }
}