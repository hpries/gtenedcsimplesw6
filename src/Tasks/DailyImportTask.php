<?php
namespace GTen\EDCSimple\Tasks;

use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;

class DailyImportTask extends ScheduledTask {
    public static function getTaskName(): string
    {
        return self::class;
    }

    public static function getDefaultInterval(): int
    {
        return 60 * 60 * 24; // daily
    }
}