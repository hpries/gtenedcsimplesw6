<?php
namespace GTen\EDCSimple\Utils;

class Fields {
    const PRODUCT_SET = 'hpr_edc_product_set';
    const CATEGORY_SET = 'hpr_edc_category_set';

    const PRODUCT_BULLETPOINTS = 'hpr_edc_product_set_bulletpoints';
    const PRODUCT_NONE_EDC = 'hpr_edc_product_set_none_edc';
    const CATEGORY_EDCID = 'hpr_edc_catgory_set_edcid';
}