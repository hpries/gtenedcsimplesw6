<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <orderdetails>
            <customerdetails>
                <email><xsl:value-of select="order/edc/connection/user"/></email>
                <apikey><xsl:value-of select="order/edc/connection/apikey"/></apikey>
                <output>advanced</output>
            </customerdetails>
            <!-- shopware customer -->
            <receiver>
                <own_ordernumber><xsl:value-of select="order/orderNumber"/></own_ordernumber>
                <name><xsl:value-of select="order/shippingAddress/firstName"/>&#160;<xsl:value-of select="order/shippingAddress/lastName"/></name>
                <xsl:if test="(order/shippingAddress/streetSimple)">
                    <street><xsl:value-of select="order/shippingAddress/streetSimple"/></street>
                </xsl:if>
                <xsl:if test="(order/shippingAddress/housenoClean)">
                    <house_nr><xsl:value-of select="order/shippingAddress/housenoClean"/></house_nr>
                </xsl:if>
                <xsl:if test="(order/shippingAddress/housenoExt)">
                    <house_nr_ext><xsl:value-of select="order/shippingAddress/housenoExt"/></house_nr_ext>
                </xsl:if>
                <postalcode><xsl:value-of select="order/shippingAddress/zipCode"/></postalcode>
                <city><xsl:value-of select="order/shippingAddress/city"/></city>

                <xsl:if test="order/shippingAddress/country/iso = 'NL'">
                    <country>1</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'BE'">
                    <country>2</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'DE'">
                    <country>3</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'UK'">
                    <country>4</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'FR'">
                    <country>5</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'LU'">
                    <country>6</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'AT'">
                    <country>7</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'PO'">
                    <country>8</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'SP'">
                    <country>9</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'CH'">
                    <country>10</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'SE'">
                    <country>11</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'IT'">
                    <country>12</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'AD'">
                    <country>13</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'AR'">
                    <country>14</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'AW'">
                    <country>15</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'BA'">
                    <country>16</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'BR'">
                    <country>17</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'BG'">
                    <country>18</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'CA'">
                    <country>19</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'HR'">
                    <country>20</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'CY'">
                    <country>21</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'CZ'">
                    <country>22</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'DK'">
                    <country>23</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'EE'">
                    <country>24</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'GR'">
                    <country>25</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'HK'">
                    <country>26</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'HU'">
                    <country>27</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'IS'">
                    <country>28</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'IE'">
                    <country>29</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'IL'">
                    <country>30</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'JP'">
                    <country>31</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'MT'">
                    <country>32</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'MX'">
                    <country>33</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'MC'">
                    <country>34</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'MA'">
                    <country>35</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'AT'">
                    <country>36</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'NZ'">
                    <country>37</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'NO'">
                    <country>38</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'PL'">
                    <country>39</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'RO'">
                    <country>40</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'SM'">
                    <country>41</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'SG'">
                    <country>42</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'SK'">
                    <country>43</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'ZA'">
                    <country>44</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'SR'">
                    <country>45</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'TW'">
                    <country>46</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'TH'">
                    <country>47</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'TR'">
                    <country>48</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'UA'">
                    <country>49</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'AE'">
                    <country>50</country>
                </xsl:if>
                <xsl:if test="order/shippingAddress/country/iso = 'US'">
                    <country>51</country>
                </xsl:if>

                <extra_email><xsl:value-of select="order/customer/email"/></extra_email>
                <phone><xsl:value-of select="order/shippingAddress/phone"/></phone>

                <xsl:if test="(order/shippingAddress/dhlid)">
                    <dhl_postid><xsl:value-of select="order/shippingAddress/dhlid"/></dhl_postid>
                </xsl:if>
            </receiver>
            <products>
                <xsl:for-each select="order/lineItems/lineItem">
                    <xsl:if test="purchaseInfo/supplierNumber != ''">
                        <xsl:for-each select="purchaseInfo/supplierNumberQuantityLists/supplierNumberQuantityList">
                            <artnr><xsl:value-of select="number"/></artnr>
                        </xsl:for-each>
                    </xsl:if>
                </xsl:for-each>
            </products>
        </orderdetails>
    </xsl:template>
</xsl:stylesheet>