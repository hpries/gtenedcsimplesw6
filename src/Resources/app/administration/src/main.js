import deDE from './snippet/de-DE';
import enGB from './snippet/en-GB';

const { Module } = Shopware;

Module.register('edc',{

    //meta
    type: 'plugin',
    name: 'edc',
    title: 'edc.label',
    description: 'edc.label',
    color: '#d0ffd0',

    //snippets
    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },
});