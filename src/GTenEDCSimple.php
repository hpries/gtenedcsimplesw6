<?php
namespace GTen\EDCSimple;

use GTen\EDCSimple\Utils\Fields;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UpdateContext;
use Shopware\Core\System\CustomField\CustomFieldTypes;

class GTenEDCSimple extends Plugin {
    private function getSetIdByName(string $name, Context $context): ?string {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('name' , $name));

        /** @var EntityRepositoryInterface $customFieldSetRepository */
        $customFieldSetRepository = $this->container->get('custom_field_set.repository');
        return $customFieldSetRepository->searchIds($criteria, $context)->firstId();
    }

    private function getFieldIdByName(string $name, Context $context): ?string {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('name' , $name));

        /** @var EntityRepositoryInterface $customFieldSetRepository */
        $customFieldSetRepository = $this->container->get('custom_field.repository');
        return $customFieldSetRepository->searchIds($criteria, $context)->firstId();
    }

    private function addFields(Context $context) {
        $customFields = [];

        //---- products
        $customFields[] =
            [
                'id' => $this->getSetIdByName(Fields::PRODUCT_SET, $context),
                'name' => Fields::PRODUCT_SET,
                'active' => true,
                'config' => [
                    'label' => [
                        'en-GB' => 'EDC - Product',
                        'de-DE' => 'EDC - Produkt'
                    ],
                ],
                'customFields' => [
                    [
                        'id' => $this->getFieldIdByName(Fields::PRODUCT_BULLETPOINTS, $context),
                        'name' => Fields::PRODUCT_BULLETPOINTS,
                        'type' => CustomFieldTypes::HTML,
                        'config' => [
                            'label' => [
                                'en-GB' => 'Bullet Points',
                                'de-DE' => 'Bullet-Points',
                            ],
                            'customFieldPosition' => 1,
                            'componentName' => 'sw-text-editor',
                            'customFieldType' => 'textEditor',
                        ],
                    ],
                    [
                        'id' => $this->getFieldIdByName(Fields::PRODUCT_NONE_EDC, $context),
                        'name' => Fields::PRODUCT_NONE_EDC,
                        'type' => CustomFieldTypes::BOOL,
                        'config' => [
                            'customFieldPosition' => 2,
                            'label' => [
                                'en-GB' => 'None EDC Product',
                                'de-DE' => 'Kein EDC Produkt',
                            ]
                        ],
                    ],
                ],
                'relations' => $this->getSetIdByName(Fields::PRODUCT_SET, $context) == null ? [
                    [
                        'entityName' => 'product'
                    ]
                ] : null,
            ];


        //---- categories
        $customFields[] =
            [
                'id' => $this->getSetIdByName(Fields::CATEGORY_SET, $context),
                'name' => Fields::CATEGORY_SET,
                'active' => true,
                'config' => [
                    'label' => [
                        'en-GB' => 'EDC - Category',
                        'de-DE' => 'EDC - Kategorie'
                    ],
                ],
                'customFields' => [
                    [
                        'id' => $this->getFieldIdByName(Fields::CATEGORY_EDCID, $context),
                        'name' => Fields::CATEGORY_EDCID,
                        'type' => CustomFieldTypes::TEXT,
                        'config' => [
                            'label' => [
                                'en-GB' => 'EDC-IDs',
                                'de-DE' => 'EDC-IDs',
                            ],
                            'customFieldPosition' => 1
                        ],
                    ],
                ],
                'relations' => $this->getSetIdByName(Fields::CATEGORY_SET, $context) == null ? [
                    [
                        'entityName' => 'category'
                    ]
                ] : null,
            ];

        $repo = $this->container->get('custom_field_set.repository');
        foreach ($customFields as $customFieldSet) {
            $repo->upsert([$customFieldSet], $context);
        }
    }

    public function install(InstallContext $installContext): void
    {
        parent::install($installContext);
        $this->addFields($installContext->getContext());
    }

    public function update(UpdateContext $updateContext): void
    {
        parent::update($updateContext);
        $this->addFields($updateContext->getContext());
    }
}