# OneDC-Plugin for Shopware >= 6.5

## Description:

This is not a finished product and still in development. You can handle all import, export actions via CLI commands. 

```
 edc
  edc:order:autoclose                                 autoclose orders
  edc:order:dump                                      ordernumber
  edc:order:export                                    ordernumber
  edc:products:disable                                disable old products
  edc:products:import                                 import XML file with edc products
  edc:products:purchaseprices                         update purchase prices
  edc:products:stock                                  update stock

```

## Releases:

you can download releases of the plugin [here](https://drive.google.com/drive/folders/11WtogGaAo6MQ0ZhNYEcTeNNyWfNwb6Rc?usp=sharing)
